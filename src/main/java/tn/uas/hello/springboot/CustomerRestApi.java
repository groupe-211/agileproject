package tn.uas.hello.springboot;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class CustomerRestApi {
	 private customerService customerService;
	   
	 @PostMapping ("/")
	    public customer save(@RequestBody customer c)
	    {
	       return (customer)customerService.creat(c);
	    	
	    }
	   
	  @GetMapping("/{id}")
	    public customer getById(@PathVariable Long id)
	    {
	      return (customer)customerService.readbyId(id);
	    }

	   @GetMapping("/")
	    public List<customer> getAll()
	    {
	    	return (List<customer>)customerService.readall(null);
	    }
	   
	    @PutMapping("/")
	    public customer update (@RequestBody customer c)
	    {
	    	if (customerService.readbyId(c.getId())!=null)
	    	{return (customer)customerService.update(c);}
	    	return null ;
	    }
	   @DeleteMapping("/{id}")
	   
	    public void deleteBuId(@PathVariable Long id)
	    {
	       customerService.deletebyId(id);
	    }

}
