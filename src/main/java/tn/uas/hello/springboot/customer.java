package tn.uas.hello.springboot;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class customer { 
	@Id 
	@GeneratedValue ( strategy = GenerationType. IDENTITY  )
	private long id;
	private String fullName;
	
	private Integer	age;
	private Boolean isStudent ;
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Boolean getIsStudent() {
		return isStudent;
	}
	public void setIsStudent(Boolean isStudent) {
		this.isStudent = isStudent;
	}
	public customer(String fullName, long id, Integer age, Boolean isStudent) {
		super();
		this.fullName = fullName;
		this.id = id;
		this.age = age;
		this.isStudent = isStudent;
	}
	


}
