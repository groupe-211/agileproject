package tn.uas.hello.springboot;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class customerServiceImp implements customerService {
@Autowired
	private customerRepository CustomerRepository;
	
	public customerServiceImp(customerRepository customerRepository) {
		super();
		CustomerRepository = customerRepository;
	}

	public customer creat(customer c) {

		return (customer)CustomerRepository.save(c);
	}

	@Override
	public customer readall(customer c) {
		
		return (customer)CustomerRepository.findAll();
	}

	public customer readbyId(Long Id) {
		Optional<customer>cusOptional=CustomerRepository.findById(Id);
		
		return cusOptional.isPresent()?cusOptional.get():null;
			
	}

	
	public customer update(customer c) {
		
		return (customer)CustomerRepository.save(c);
	}

	public void deletebyId(Long Id) {
	
		 CustomerRepository.deleteById(Id);
	}


	public void deleteALL () {

		CustomerRepository.deleteAll();
	}



	
	

	



	
}
